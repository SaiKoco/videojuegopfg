package com.simion.base;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Simion on 02/06/2017.
 */
public class BolaTest {
    @Test
    public void testing() {
        Bola bola = new Bola(20, 10, true);
        Rectangle rect = bola.rect;
        assertEquals(20, (int)rect.x);
        assertEquals(10, (int)rect.y);
    }
}