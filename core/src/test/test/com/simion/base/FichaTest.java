package com.simion.base;

import com.badlogic.gdx.math.Vector2;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Simion on 02/06/2017.
 */
public class FichaTest {
    @Test
    public void testing() {
        Ficha ficha = new Ficha(10, 20,true);
        Vector2 posicion = ficha.posicion;
        assertEquals(10, (int)posicion.x);
        assertEquals(20, (int)posicion.y);
    }
}