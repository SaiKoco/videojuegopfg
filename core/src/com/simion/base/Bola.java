package com.simion.base;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.simion.resources.ResourcesManager;

/**
 * Created by Simion on 16/05/2017.
 */
public class Bola{
    protected TextureRegion imagen;
    public Rectangle rect;
    public String rutaImagen;
    public boolean tocada;
    public int numCapa;
    public boolean visible;

    public Bola(float x, float y, String rutaImagen) {
        this.rutaImagen = rutaImagen;
        Texture img = new Texture(this.rutaImagen);
        imagen = new TextureRegion(img, img.getWidth(), img.getHeight());
        rect = new Rectangle(x, y, imagen.getRegionWidth(), imagen.getRegionHeight());
        numCapa = 1;
        visible = true;
        tocada = false;
    }

    public Bola(float x, float y, boolean isTesting){
        rect = new Rectangle(x, y, 1, 1);
    }

    public void render(Batch batch) {
        if (visible){
            batch.draw(imagen, rect.x, rect.y);
        }
    }

    public TextureRegion getImagen(){
        return imagen;
    }

    public void dispose() {
    }
}
