package com.simion.base;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import javax.xml.soap.Text;

import static com.simion.util.Constantes.RUTA_FICHA_RED;

/**
 * Created by Simion on 30/05/2017.
 */
public class Ficha {
    protected TextureRegion imagen;
    public Vector2 posicion;
    public Rectangle rect;
    public String rutaImagen;
    public boolean tocada;
    public boolean cambiada;

    public Ficha(float x, float y) {
        this.rutaImagen = "";
        posicion = new Vector2(x, y);
        Texture img = new Texture(RUTA_FICHA_RED);
        imagen = new TextureRegion(img, img.getWidth(), img.getHeight());
        rect = new Rectangle(x, y, imagen.getRegionWidth(), imagen.getRegionHeight());
        imagen = new TextureRegion(new Texture(new Pixmap(1, 1, Pixmap.Format.RGB565)));
        tocada = false;
    }
    public Ficha(float x, float y, boolean isTest) {
        posicion = new Vector2(x, y);
    }

    public TextureRegion getImagen() {
        return imagen;
    }

    public void setImagen(String rutaImagen){
        if (rutaImagen.equals("")){
            imagen = new TextureRegion(new Texture(new Pixmap(1, 1, Pixmap.Format.RGB565)));
            cambiada = false;
        }
        else{
            Texture img = new Texture(rutaImagen);
            imagen = new TextureRegion(img, img.getWidth(), img.getHeight());
        }
    }

    public void setImagen(TextureRegion imagen) {
        this.imagen = imagen;
        cambiada = true;
    }
}
