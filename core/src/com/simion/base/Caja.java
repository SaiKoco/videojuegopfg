package com.simion.base;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.simion.resources.ResourcesManager;

import static com.simion.util.Constantes.RUTA_CAJA_AMARILLA;
import static com.simion.util.Constantes.RUTA_CAJA_NEGRA;

/**
 * Created by Simion on 28/05/2017.
 */
public class Caja {
    protected TextureRegion imagen;
    public Vector2 posicion;
    public Rectangle rect;
    public long tiempo;
    public boolean amarilla;

    public Caja(float x, float y, String nombreImagen, int width, int heigth) {
        imagen = new TextureRegion(new Texture(nombreImagen), width, heigth);
        posicion = new Vector2(x, y);
        rect = new Rectangle(x, y, imagen.getRegionWidth(), imagen.getRegionHeight());
        tiempo = 0;
        amarilla = false;
    }

    public TextureRegion getImagen(){
        return imagen;
    }

    public void setImagen(TextureRegion imagen) {
        this.imagen = imagen;
    }
}
