package com.simion.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.*;
import com.simion.Lanzador;

import static com.simion.util.Constantes.*;
import static com.simion.util.Constantes.RUTA_BT_ATRAS;

/**
 * Created by Simion on 01/06/2017.
 */
public class CreditosScreen implements Screen {

    private final Lanzador juego;
    Stage escena;
    private OrthographicCamera camara;
    OrthogonalTiledMapRenderer mapRenderer;

    public CreditosScreen(Lanzador juego){
        this.juego = juego;
        Preferences preferences = Gdx.app.getPreferences("juego_settings");
        String resolucion = preferences.getString("resolucion");
        Boolean fullScreen = preferences.getBoolean("pantallaCompleta");
        if (!fullScreen){
            if(resolucion.equals(_1024x768))
                Gdx.graphics.setWindowedMode(1024, 768);
            else if(resolucion.equals(_800x600))
                Gdx.graphics.setWindowedMode(800, 600);
            else if(resolucion.equals(_640x480))
                Gdx.graphics.setWindowedMode(640, 480);
        }
        else {
            Graphics.DisplayMode displayMode = Gdx.graphics.getDisplayMode(Gdx.graphics.getPrimaryMonitor());
            Gdx.graphics.setFullscreenMode(displayMode);
        }
    }

    @Override
    public void show() {

        if (!VisUI.isLoaded()){
            VisUI.load();
        }
        camara = new OrthographicCamera();
        camara.setToOrtho(false, 640, 480);
        camara.update();
        TiledMap mapa = new TmxMapLoader().load(RUTA_CREDITOS);
        mapRenderer = new OrthogonalTiledMapRenderer(mapa);
        juego.batch = (SpriteBatch) mapRenderer.getBatch();

        mapRenderer.setView(camara);

        escena = new Stage();
        VisTable tabla = new VisTable();
        tabla.setPosition(30, Gdx.graphics.getHeight() - 30);
        escena.addActor(tabla);

        Drawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_ATRAS)));
        VisImageButton btAtras = new VisImageButton(drawable);
        btAtras.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new MenuScreen(juego));
            }
        });
        tabla.row();
        tabla.add(btAtras).center().pad(5).width(45).height(45);

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_CREDITOS_TEXTO)));
        VisImage visImage = new VisImage(drawable);
        visImage.setPosition(Gdx.graphics.getWidth()/2 - visImage.getWidth()/2, Gdx.graphics.getHeight()/2 - visImage.getHeight()/2);
        escena.addActor(visImage);
        // Activa el input de usuario para la escena
        Gdx.input.setInputProcessor(escena);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mapRenderer.render();
        juego.batch.begin();

        juego.batch.end();
        escena.act(delta);
        escena.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
