package com.simion.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.*;
import com.simion.Lanzador;
import com.simion.resources.ResourcesManager;
import com.sun.prism.image.ViewPort;

import static com.simion.util.Constantes.*;

/**
 * Created by Simion on 27/04/2017.
 */
public class MenuScreen implements Screen {

    private final Lanzador juego;
    Stage escena;
    private OrthographicCamera camara;
    OrthogonalTiledMapRenderer mapRenderer;
    public MenuScreen(Lanzador juego){
        this.juego = juego;
        Preferences preferences = Gdx.app.getPreferences("juego_settings");
        String resolucion = preferences.getString("resolucion");
        Boolean fullScreen = preferences.getBoolean("pantallaCompleta");
        Boolean musica = preferences.getBoolean("musica");
        Float volumen = preferences.getFloat("volumen");
        if (musica){
            final Music music = ResourcesManager.obtenerMusica(FILE_MUSIC);
            music.setVolume(volumen/100);
            music.play();
        }
        if (!fullScreen){
            if(resolucion.equals(_1024x768))
                Gdx.graphics.setWindowedMode(1024, 768);
            else if(resolucion.equals(_800x600))
                Gdx.graphics.setWindowedMode(800, 600);
            else if(resolucion.equals(_640x480))
                Gdx.graphics.setWindowedMode(640, 480);
        }
        else {
            Graphics.DisplayMode displayMode = Gdx.graphics.getDisplayMode(Gdx.graphics.getPrimaryMonitor());
            Gdx.graphics.setFullscreenMode(displayMode);
        }
    }

    @Override
    public void show() {

        if (!VisUI.isLoaded()){
            VisUI.load();
        }
        camara = new OrthographicCamera();
        camara.setToOrtho(false, 1600, 1200);
        camara.update();
        TiledMap mapa = new TmxMapLoader().load(FONDO_MAIN);
        mapRenderer = new OrthogonalTiledMapRenderer(mapa);
        juego.batch = (SpriteBatch) mapRenderer.getBatch();

        mapRenderer.setView(camara);

        // Crea la escena
        escena = new Stage();
        // Crea una tabla que servirá de layout para los componentes de la UI
        VisTable tabla = new VisTable();
        tabla.setFillParent(true);
        // Añade la tabla a la escena
        escena.addActor(tabla);

        Drawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_JUEGO_ABACO)));
        VisImageButton btAbaco = new VisImageButton(drawable);
        btAbaco.setSize(300, 225);
        btAbaco.setPosition(50, Gdx.graphics.getHeight() / 2 - btAbaco.getHeight()/2);
        btAbaco.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new AbacoScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_JUEGO_TABLERO)));
        VisImageButton btTablero = new VisImageButton(drawable);
        btTablero.setSize(300, 225);
        btTablero.setPosition(Gdx.graphics.getWidth()/2 - btTablero.getWidth()/2, Gdx.graphics.getHeight() / 2 - btTablero.getHeight()/2);
        btTablero.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new TableroScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_JUEGO_FICHAS)));
        VisImageButton btFichas = new VisImageButton(drawable);
        btFichas.setSize(300, 225);
        btFichas.setPosition(Gdx.graphics.getWidth()-btFichas.getWidth()-50, Gdx.graphics.getHeight() / 2 - btFichas.getHeight()/2);
        btFichas.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new FichasScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });

        VisTextButton btConfig = new VisTextButton("Configuración");
        btConfig.setSize(150, 50);
        btConfig.setPosition(Gdx.graphics.getWidth()/2 - btConfig.getWidth()/2, 20);
        btConfig.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new ConfigScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_ATRAS)));
        VisImageTextButton btSalir = new VisImageTextButton("Salir", drawable);
        btSalir.setSize(110, 50);
        btSalir.setPosition(20, 20);
        btSalir.getLabel().setFontScale(1.1f);
        btSalir.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                // Qué hacer al pulsar el botón
                juego.dispose();
                System.exit(0);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                // Qué hacer al soltar el botón
            }
        });

        VisTextButton btCreditos = new VisTextButton("Creditos");
        btCreditos.setSize(120, 50);
        btCreditos.setPosition(Gdx.graphics.getWidth() - btCreditos.getWidth() - 20, 20);
        btCreditos.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new CreditosScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_TITULO)));
        VisImage titulo = new VisImage(drawable);
        titulo.setSize(500, 200);
        titulo.setPosition(Gdx.graphics.getWidth()/2 - titulo.getWidth()/2, Gdx.graphics.getHeight() - titulo.getHeight() - 30);
        escena.addActor(titulo);
        escena.addActor(btAbaco);
        escena.addActor(btTablero);
        escena.addActor(btFichas);
        escena.addActor(btConfig);
        escena.addActor(btCreditos);
        escena.addActor(btSalir);

        // Activa el input de usuario para la escena
        Gdx.input.setInputProcessor(escena);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mapRenderer.render();
        juego.batch.begin();

        juego.batch.end();
        escena.act(delta);
        escena.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        VisUI.dispose();
        escena.dispose();
    }
}
