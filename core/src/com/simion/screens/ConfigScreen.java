package com.simion.screens;

import com.badlogic.gdx.*;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.widget.*;
import com.simion.Lanzador;
import com.simion.resources.ResourcesManager;
import com.sun.org.apache.regexp.internal.RE;

import static com.simion.util.Constantes.*;

/**
 * Created by Simion on 09/03/2017.
 */
public class ConfigScreen implements Screen {

    private final Lanzador juego;
    private Stage stage;
    private VisCheckBox cbSonido, cbMusica, cbPantallaCompleta;
    private VisSlider slVolumen;
    private VisSelectBox sbResolucion;

    public ConfigScreen(Lanzador juego) {
        this.juego = juego;
    }

    private void cargarPreferencias() {

        Preferences preferences = Gdx.app.getPreferences("juego_settings");
        cbSonido.setChecked(preferences.getBoolean("sonido"));
        cbMusica.setChecked(preferences.getBoolean("musica"));
        cbPantallaCompleta.setChecked(preferences.getBoolean("pantallaCompleta"));
        slVolumen.setValue(preferences.getFloat("volumen"));
        sbResolucion.setSelected(preferences.getString("resolucion"));
    }

    private void guardarPreferencias() {

        Preferences preferences = Gdx.app.getPreferences("juego_settings");
        preferences.putBoolean("sonido", cbSonido.isChecked());
        preferences.putBoolean("musica", cbMusica.isChecked());
        preferences.putBoolean("pantallaCompleta", cbPantallaCompleta.isChecked());
        preferences.putFloat("volumen", slVolumen.getValue());
        preferences.putString("resolucion", (String) sbResolucion.getSelected());
        preferences.flush();
    }


    @Override
    public void show() {

        stage = new Stage();
        VisTable tabla = new VisTable();
        tabla.setFillParent(true);
        stage.addActor(tabla);
        final Music music = ResourcesManager.obtenerMusica(FILE_MUSIC);

        cbSonido = new VisCheckBox("SONIDO");
        cbSonido.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

            }
        });

        slVolumen = new VisSlider(0, 100, 1, false);
        slVolumen.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                music.setVolume(slVolumen.getValue()/100);
            }
        });
        tabla.addActor(slVolumen);
        music.setVolume(slVolumen.getValue()/100);

        cbMusica = new VisCheckBox("Musica");
        cbMusica.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (cbMusica.isChecked()){
                    music.play();
                }
                else
                    music.stop();

            }
        });

        sbResolucion = new VisSelectBox();
        sbResolucion.setItems(_1024x768, _800x600, _640x480);

        cbPantallaCompleta = new VisCheckBox("PANTALLA COMPLETA");
        cbPantallaCompleta.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

            }
        });

        VisTextButton btHecho = new VisTextButton("HECHO");
        btHecho.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                guardarPreferencias();

                if (!cbPantallaCompleta.isChecked()){
                    if(sbResolucion.getSelected().equals(_1024x768))
                        Gdx.graphics.setWindowedMode(1024, 768);
                    else if(sbResolucion.getSelected().equals(_800x600))
                        Gdx.graphics.setWindowedMode(800, 600);
                    else if(sbResolucion.getSelected().equals(_640x480))
                        Gdx.graphics.setWindowedMode(640, 480);

                }
                else {
                    Graphics.DisplayMode displayMode = Gdx.graphics.getDisplayMode(Gdx.graphics.getPrimaryMonitor());
                    Gdx.graphics.setFullscreenMode(displayMode);
                }
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen(juego));
                dispose();
            }
        });

        tabla.row();
        tabla.add(cbSonido).left().width(200).height(20).pad(10);
        tabla.row();
        tabla.add(cbMusica).left().width(200).height(20).pad(10);
        tabla.row();
        tabla.add(slVolumen).center().width(200).height(20).pad(10);
        tabla.row();
        tabla.add(sbResolucion).center().width(200).height(20).pad(10);
        tabla.row();
        tabla.add(cbPantallaCompleta).left().width(200).height(20).pad(15);
        tabla.row();
        tabla.add(btHecho).center().width(200).height(50).pad(5);

        cargarPreferencias();
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 0.2f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
