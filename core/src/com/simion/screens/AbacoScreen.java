package com.simion.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.simion.Lanzador;
import com.simion.base.Bola;

import java.util.ArrayList;

import static com.simion.util.Constantes.*;

public class AbacoScreen implements Screen{
    private final Lanzador juego;
    private Batch batch;
    BitmapFont fuente;
    TiledMap mapa;
    OrthogonalTiledMapRenderer mapRenderer;
    ArrayList<Bola> bolas;
    ArrayList<Bola> bolas_blancas;
    private MapObjects listaExtremos;
    private OrthographicCamera camara;
    private Stage escena;
    private int numFilas;
    private Rectangle rectMouse;
    private int[] layerCapas;

    public AbacoScreen(Lanzador juego) {
        this.juego = juego;
        batch = juego.batch;
        fuente = fuente;
        numFilas = 1;
    }

    @Override
    public void show() {
        camara = new OrthographicCamera();
        camara.setToOrtho(false,
                ANCHURA_CAMARA * SIZE_TILED,
                ALTURA_CAMARA * SIZE_TILED);
//        camara.setToOrtho(false,
//                ANCHURA,
//                ALTURA);
        camara.update();
        bolas_blancas = new ArrayList<Bola>();
        bolas = new ArrayList<Bola>();

        mapa = new TmxMapLoader().load(RUTA_ABACO);
        layerCapas = new int[]{0, 2, 11};

        for (int i = 1; i< 10; i++){
            MapLayer capaObjetos = mapa.getLayers().get("Bolas_"+i);
            MapObjects mapObjects = capaObjetos.getObjects();
            for (MapObject mapObject : mapObjects) {
                TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObject;
                if (tileMapObject.getName().equals("Bola_roja")) {
                    Bola bola = new Bola(tileMapObject.getX(), tileMapObject.getY(), RUTA_BOLA_ROJA);
                    bola.numCapa = i;
                    bolas.add(bola);
                }
                else if (tileMapObject.getName().equals("Bola_blanca")){
                    Bola bola = new Bola(tileMapObject.getX(), tileMapObject.getY(), RUTA_BOLA_BLANCA);
                    bola.numCapa = i;
                    bolas.add(bola);
                }
            }
            if (i > 1){
                for (Bola bola: bolas){
                    if (bola.numCapa == i){
                        bola.visible = false;
                    }
                }
            }

        }

        mapRenderer = new OrthogonalTiledMapRenderer(mapa);
        batch = mapRenderer.getBatch();
        mapRenderer.setView(camara);

        final MapLayer capaColision = mapa.getLayers().get(CP_COLISION);
        listaExtremos = capaColision.getObjects();

        // Crea la escena
        escena = new Stage();
        VisTable tabla = new VisTable();
        tabla.setPosition(30, Gdx.graphics.getHeight() - 30);
        VisTable tabla2 = new VisTable();
        tabla2. setPosition(140, Gdx.graphics.getHeight() - 30);
        VisTable tabla3 = new VisTable();
        tabla3. setPosition(285, Gdx.graphics.getHeight() - 30);
        VisTable tabla4 = new VisTable();
        tabla4. setPosition(335, Gdx.graphics.getHeight() - 30);
        escena.addActor(tabla);
        escena.addActor(tabla2);
        escena.addActor(tabla3);
        escena.addActor(tabla4);

        Drawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_ATRAS)));
        VisImageButton btAtras = new VisImageButton(drawable);
        btAtras.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new MenuScreen(juego));
            }
        });
        tabla.row();
        tabla.add(btAtras).center().pad(5).width(45).height(45);

        VisTextButton btReiniciar = new VisTextButton("Reiniciar");
        btReiniciar.setSize(60, 60);
        btReiniciar.setColor(com.badlogic.gdx.graphics.Color.WHITE);
        btReiniciar.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new AbacoScreen(juego));
            }
        });
        tabla2.row();
        tabla2.add(btReiniciar).right().pad(5).width(150).height(45);

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_ADD)));
        VisImageButton btAdd = new VisImageButton(drawable);
        btAdd.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if(numFilas<9){
                    numFilas ++;
                    //mapa.getLayers().get("Bolas_"+numFilas).setVisible(true);
                    layerCapas = new int[numFilas+1];
                    layerCapas[0] = 0;
                    layerCapas[1] = 2;
                    for(int i = 2; i<=numFilas; i++){
                        layerCapas[i] = i+1;
                    }
//                    for (int i = 0; i< layerCapas.length; i++){
//                        System.out.print(layerCapas[i]+ " ");
//                    }
//                    System.out.println();
                    for (Bola bola: bolas){
                        if (bola.numCapa == numFilas){
                            bola.visible = true;
                        }
                    }
                }
            }
        });
        tabla3.row();
        tabla3.add(btAdd).center().pad(5).width(45).height(45);

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_MINUS)));
        VisImageButton btMinus = new VisImageButton(drawable);
        btMinus.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (numFilas > 1){
                    //mapa.getLayers().get("Bolas_"+numFilas).setVisible(false);
                    layerCapas = new int[numFilas];
                    layerCapas[0] = 0;
                    layerCapas[1] = 2;
                    for(int i = 2; i<numFilas; i++){
                        layerCapas[i] = i+1;
                    }
//                    for (int i = 0; i< layerCapas.length; i++){
//                        System.out.print(layerCapas[i]+ " ");
//                    }
//                    System.out.println();
                    for (Bola bola: bolas){
                        if (bola.numCapa == numFilas){
                            bola.visible = false;
                        }
                    }
                    numFilas--;
                }
            }
        });
        tabla4.row();
        tabla4.add(btMinus).center().pad(5).width(45).height(45);

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_AYUDA_AB)));
        final VisImageButton imagenAyuda = new VisImageButton(drawable);
        imagenAyuda.setSize(656, 520);
        imagenAyuda.setPosition(Gdx.graphics.getWidth()/2 - imagenAyuda.getWidth()/2, Gdx.graphics.getHeight()/2 - imagenAyuda.getHeight()/2);
        imagenAyuda.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                imagenAyuda.setVisible(false);
            }
        });
        imagenAyuda.setVisible(false);
        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_AYUDA), 100, 100));
        VisImageButton btAyuda = new VisImageButton(drawable);
        btAyuda.setSize(45, 45);
        btAyuda.setPosition(Gdx.graphics.getWidth()/2 - btAyuda.getWidth()/2, Gdx.graphics.getHeight()- btAyuda.getHeight() - 10);
        btAyuda.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                imagenAyuda.setVisible(true);
            }
        });

        VisTable tablaAyuda = new VisTable();
        tablaAyuda.setPosition(Gdx.graphics.getWidth() - 30, Gdx.graphics.getHeight()-30);
        tablaAyuda.add(btAyuda).width(45).height(45);
        escena.addActor(tablaAyuda);
        escena.addActor(imagenAyuda);

        // Activa el input de usuario para la escena
        Gdx.input.setInputProcessor(escena);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mapRenderer.render(layerCapas);
        batch.begin();
        for (Bola bola: bolas){
            bola.render(batch);
        }
        batch.end();
        escena.act(delta);
        escena.draw();

        ComprobarRaton(delta);
        comprobarColision();

    }

    private void comprobarColision() {
//        for(int i = 1; i < bolas.size(); i++){
//            if(bolas.get(i).rect.overlaps(bolas.get(i-1).rect)) {
//                bolas.get(i).rect.x = bolas.get(i - 1).rect.x - bolas.get(i).rect.getWidth();
//            }
//            if(i > 0){
//                if(bolas.get(i).rect.overlaps(bolas.get(i-1).rect)){
//                    if (bolas.get(i).tocada){
//                        bolas.get(i-1).posicion.x = bolas.get(i).posicion.x + bolas.get(i).rect.getWidth();
//                    }
//                    else {
//                        bolas.get(i).posicion.x = bolas.get(i-1).posicion.x - bolas.get(i).rect.getWidth();
//                    }
//                }
//            }
//            if(i < bolas.size()-1){
//                if(bolas.get(i+1).rect.overlaps(bolas.get(i).rect)){
//                    if (bolas.get(i+1).tocada){
//                        bolas.get(i).posicion.x = bolas.get(i+1).posicion.x + bolas.get(i+1).rect.getWidth();
//                    }
//                    else {
//                        bolas.get(i+1).posicion.x = bolas.get(i).posicion.x - bolas.get(i+1).rect.getWidth();
//                    }
//                }
//            }
 //       }
        for (MapObject mapObject : listaExtremos) {
            Rectangle rect = ((RectangleMapObject) mapObject).getRectangle();
            for (Bola bola: bolas){
                for (Bola bola2: bolas){
                    if (bola.rect.overlaps(bola2.rect) ){
                        if (!bola.equals(bola2)){
                            if(bola.rect.x < bola2.rect.x){
                                bola2.rect.x = bola.rect.x + bola.rect.getWidth();
                            }
                            else
                                bola2.rect.x = bola.rect.x - bola2.rect.getWidth();
                        }
                    }
                }
                if (mapObject.getName().equals("izquierda")) {
                    if (bola.rect.overlaps(rect)){
                        bola.rect.x = rect.x + rect.getWidth();
                    }
                    if (bola.rect.x < rect.x - rect.getWidth()){
                        bola.rect.x = rect.x + rect.getWidth();
                    }
                }

                else if (mapObject.getName().equals("derecha")) {
                    if (bola.rect.overlaps(rect)) {
                        bola.rect.x = rect.x - bola.rect.getWidth();
                    }
                    if (bola.rect.x > rect.x - bola.rect.getWidth()){
                        bola.rect.x = rect.x - bola.rect.getWidth();
                    }

                }
            }
        }
    }

    private void ComprobarRaton(float delta) {
        if (Gdx.input.isTouched()){
            Vector3 posicion = new Vector3();
            posicion.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camara.unproject(posicion);
            rectMouse = new Rectangle(posicion.x - 5, posicion.y - 5, 10, 10);
            for(Bola bola: bolas){
                //System.out.println(rectMouse.x +" - "+ bola.rect.x + "  " + rectMouse.y + " - " + bola.rect.y);
                if (rectMouse.overlaps(bola.rect) && !bola.tocada ){
                    bola.tocada = true;
                }
                if (bola.tocada){
                    bola.rect.x = rectMouse.x +5 - bola.getImagen().getRegionWidth() / 2;
                }
            }
        }
        else {
            for (Bola bola: bolas){
                bola.tocada = false;
            }
        }
//        if (!Gdx.input.justTouched() && bolaTocada){
//            System.out.println("soltado");
//            bolaTocada = false;
//        }
    }

    @Override
    public void resize(int width, int height) {
        camara.viewportHeight = height;
        camara.viewportWidth = width;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}