package com.simion.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.widget.*;
import com.simion.Lanzador;
import com.simion.base.Caja;
import com.simion.base.Ficha;
import com.simion.resources.ResourcesManager;

import static com.simion.util.Constantes.*;

/**
 * Created by Simion on 30/05/2017.
 */
public class FichasScreen implements Screen {
    private final Lanzador juego;
    private Batch batch;
    TiledMap mapa;
    OrthogonalTiledMapRenderer mapRenderer;
    Array<Ficha> fichas;
    private OrthographicCamera camara;
    private Stage escena;
    private Rectangle rectMouse;
    private Rectangle fichaRoja;
    private Rectangle fichaAmarilla;
    private boolean permisoTocar;
    private Ficha fichaMovil;
    private boolean tocando;
    private int numFichas;
    private boolean finPartida;
    private  VisTable tablaFin;
    private VisTable tablaError;
    private boolean autoRellenado;
    private boolean comprobar;
    private boolean correcto;

    public FichasScreen(Lanzador juego){
        this.juego = juego;
        batch = juego.batch;
        fichaMovil = new Ficha(0,0);
        numFichas = MathUtils.random(1, 12);
    }

    @Override
    public void show() {
        camara = new OrthographicCamera();
        camara.setToOrtho(false, 1280, 1024);
        camara.update();

        fichas = new Array<Ficha>();

        mapa = new TmxMapLoader().load(RUTA_TABFICHAS);
        mapRenderer = new OrthogonalTiledMapRenderer(mapa);
        batch = mapRenderer.getBatch();

        mapRenderer.setView(camara);
        MapLayer capaObjetos = mapa.getLayers().get("Fichas");
        MapObjects mapObjects = capaObjetos.getObjects();
        for (MapObject mapObject : mapObjects) {
            TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObject;
            Ficha ficha = new Ficha(tileMapObject.getX(), tileMapObject.getY());
            fichas.add(ficha);
        }

        capaObjetos = mapa.getLayers().get("Fichas_Fijas");
        mapObjects = capaObjetos.getObjects();
        for (MapObject mapObject : mapObjects) {
            Rectangle rect = ((RectangleMapObject) mapObject).getRectangle();
            if (mapObject.getName().equals(FICHA_ROJA)){
                fichaRoja = rect;
            }
            else if (mapObject.getName().equals(FICHA_AMARILLA)){
                fichaAmarilla = rect;
            }
        }

        escena = new Stage();
        final VisTable tabla = new VisTable();
        tabla.setPosition(30, Gdx.graphics.getHeight() - 30);
        VisTable tabla2 = new VisTable();
        tabla2.setPosition(140,Gdx.graphics.getHeight() - 30);
        VisTable tabla3 = new VisTable();
        tabla3.setPosition(260, Gdx.graphics.getHeight() - 90);
        VisTable tabla4 = new VisTable();
        tabla4.setPosition(Gdx.graphics.getWidth() - 170, Gdx.graphics.getHeight() - 30);
        escena.addActor(tabla);
        escena.addActor(tabla2);
        escena.addActor(tabla3);
        escena.addActor(tabla4);
        rectMouse = new Rectangle(0,0,10,10);

        Drawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_ATRAS)));
        VisImageButton btAtras = new VisImageButton(drawable);
        btAtras.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new MenuScreen(juego));
            }
        });
        tabla.row();
        tabla.add(btAtras).center().pad(5).width(45).height(45);

        VisTextButton btReiniciar = new VisTextButton("Reiniciar");
        btReiniciar.setSize(60, 60);
        btReiniciar.setColor(com.badlogic.gdx.graphics.Color.WHITE);
        btReiniciar.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new FichasScreen(juego));
            }
        });
        tabla2.row();
        tabla2.add(btReiniciar).left().pad(5).width(150).height(45);

        VisLabel vsLabel = new VisLabel("Pon " + numFichas + " fichas en la tabla.");
        drawable = new TextureRegionDrawable(new TextureRegion(new Texture("fichasColores/numFichas/numFichas_" + numFichas + ".png")));
        VisImage vsImagenFichas = new VisImage(drawable);
        vsLabel.setColor(Color.BLACK);
        vsLabel.setFontScale(1.1f,1.1f);
        tabla3.row();
        tabla3.add(vsImagenFichas).left().width(500).height(40);

        final VisTextButton btAutoFill = new VisTextButton("Autorellenar Tabla");
        btAutoFill.setSize(60, 60);
        btAutoFill.setColor(com.badlogic.gdx.graphics.Color.WHITE);
        btAutoFill.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                autoRellenado = true;
                int numRell = 0;
                while(numFichas != numRell ){
                    int numRand = MathUtils.random(0, fichas.size-1);
                    if (numRell > numFichas){
                        if(fichas.get(numRand).getImagen().getRegionWidth() > 1){
                            fichas.get(numRand).setImagen("");
                        }
                    }
                    else{
                        if(fichas.get(numRand).getImagen().getRegionWidth() == 1){
                            int numRand2 = MathUtils.random(0, 1);
                            if (numRand2 == 0){
                                fichas.get(numRand).setImagen(RUTA_FICHA_RED);
                            }
                            else {
                                fichas.get(numRand).setImagen(RUTA_FICHA_YELLOW);
                            }
                        }
                    }
                    numRell = 0;
                    for (Ficha ficha: fichas){
                        if (ficha.getImagen().getRegionWidth() > 1){
                            numRell++;
                        }
                    }
                }
                btAutoFill.setVisible(false);
            }
        });
        VisTextButton btComprobar = new VisTextButton("Comprobar resultado");
        btComprobar.setColor(com.badlogic.gdx.graphics.Color.WHITE);
        btComprobar.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                comprobar = true;
                int cont = 0;
                for (Ficha ficha: fichas){
                    if (ficha.getImagen().getRegionWidth() > 1){
                        cont++;
                    }
                }
                if (cont == numFichas){
                    correcto = true;
                }
                else
                    correcto = false;
            }
        });
        tabla4.row();
        tabla4.add(btAutoFill).left().pad(5).width(150).height(45);
        tabla4.add(btComprobar).left().pad(5).width(150).height(45);

        tablaFin = new VisTable();
        tablaFin.setFillParent(true);
        // Añade la tabla a la escena
        escena.addActor(tablaFin);

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_FIN_RETO)));
        VisImage vsFinImagen = new VisImage(drawable);
        VisTextButton btRestart = new VisTextButton("Reiniciar Partida");
        btRestart.setSize(200, 60);
        btRestart.setPosition(tabla.getWidth()/2 - btRestart.getWidth()/2, 40);
        btRestart.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new FichasScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });
        VisTextButton btMenuInicio = new VisTextButton("Volver al Menu Principal");
        btMenuInicio.setSize(200, 60);
        btMenuInicio.setPosition(tabla.getWidth()/2 - btMenuInicio.getWidth()/2, 40);
        btMenuInicio.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new MenuScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });
        tablaFin.row();
        tablaFin.add(vsFinImagen).center().pad(5);
        tablaFin.row();
        tablaFin.add(btRestart).center().pad(5).width(300).height(80);;
        tablaFin.row();
        tablaFin.add(btMenuInicio).center().pad(5).width(300).height(80);;
        tablaFin.setVisible(false);

        tablaError = new VisTable();
        tablaError.setFillParent(true);
        // Añade la tabla a la escena
        escena.addActor(tablaError);



        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_ERROR_RETO)));
        VisImage vsErrorImagen = new VisImage(drawable);
        VisTextButton btErrorRestart = new VisTextButton("Reiniciar Partida");
        btErrorRestart.setSize(200, 60);
        btErrorRestart.setPosition(tabla.getWidth()/2 - btErrorRestart.getWidth()/2, 40);
        btErrorRestart.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new FichasScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_ERROR_RETO)));
        VisTextButton btReintentar = new VisTextButton("Reintentar");
        btReintentar.setSize(200, 60);
        btReintentar.setPosition(tabla.getWidth()/2 - btReintentar.getWidth()/2, 40);
        btReintentar.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                tablaError.setVisible(false);
                comprobar = false;
                autoRellenado = false;
                btAutoFill.setVisible(true);
                for (Ficha ficha: fichas){
                    ficha.setImagen("");
                }
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });
        VisTextButton btErrorMain = new VisTextButton("Volver al Menu Principal");
        btErrorMain.setSize(200, 60);
        btErrorMain.setPosition(tabla.getWidth()/2 - btErrorMain.getWidth()/2, 40);
        btErrorMain.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new MenuScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });
        VisTextButton btErrorReiniciar = new VisTextButton("Reiniciar");
        btErrorReiniciar.setSize(60, 60);
        btErrorReiniciar.setColor(com.badlogic.gdx.graphics.Color.WHITE);
        btErrorReiniciar.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new FichasScreen(juego));
            }
        });
        tablaError.row();
        tablaError.add(vsErrorImagen).center().pad(5);
        tablaError.row();
        tablaError.add(btReintentar).center().pad(5).width(300).height(80);;
        tablaError.row();
        tablaError.add(btErrorReiniciar).center().pad(5).width(300).height(80);
        tablaError.row();
        tablaError.add(btErrorMain).center().pad(5).width(300).height(80);;
        tablaError.setVisible(false);

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_AYUDA_FH)));
        final VisImageButton imagenAyuda = new VisImageButton(drawable);
        imagenAyuda.setSize(656, 520);
        imagenAyuda.setPosition(Gdx.graphics.getWidth()/2 - imagenAyuda.getWidth()/2, Gdx.graphics.getHeight()/2 - imagenAyuda.getHeight()/2);
        imagenAyuda.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                imagenAyuda.setVisible(false);
            }
        });
        imagenAyuda.setVisible(false);
        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_AYUDA), 100, 100));
        VisImageButton btAyuda = new VisImageButton(drawable);
        btAyuda.setSize(45, 45);
        btAyuda.setPosition(Gdx.graphics.getWidth()/2 - btAyuda.getWidth()/2, Gdx.graphics.getHeight()- btAyuda.getHeight() - 10);
        btAyuda.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                imagenAyuda.setVisible(true);
            }
        });

        VisTable tablaAyuda = new VisTable();
        tablaAyuda.setPosition(Gdx.graphics.getWidth()/2 - tablaAyuda.getWidth()/2, Gdx.graphics.getHeight()-30);
        tablaAyuda.add(btAyuda).width(45).height(45);
        escena.addActor(tablaAyuda);
        escena.addActor(imagenAyuda);

        Gdx.input.setInputProcessor(escena);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mapRenderer.render();
        batch.begin();
        //batch.draw(foto, rectMouse.x, rectMouse.y);
        for(Ficha ficha: fichas){
            batch.draw(ficha.getImagen(), ficha.posicion.x, ficha.posicion.y);
        }
        batch.draw(fichaMovil.getImagen(), fichaMovil.posicion.x, fichaMovil.posicion.y);
        batch.end();
        escena.act(delta);
        escena.draw();

        if (!autoRellenado && !comprobar){
            comprobarInput();
            comprobarFichas();
        }
        if (correcto && comprobar){
            tablaFin.setVisible(true);
        }
        else if (comprobar){
            tablaError.setVisible(true);
        }

    }

    private void comprobarFichas() {
        int fichasEnTabla = 0;
        for (Ficha ficha: fichas){
            if (ficha.getImagen().getRegionWidth() > 1){
                fichasEnTabla ++;
            }
        }
    }

    private void comprobarInput() {
        if (Gdx.input.isTouched()){
            tocando = true;
            Vector3 posicion = new Vector3();
            posicion.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camara.unproject(posicion);
            rectMouse = new Rectangle(posicion.x - 5, posicion.y - 5, 10, 10);
            if(rectMouse.overlaps(fichaRoja) && fichaMovil.getImagen().getRegionWidth() == 1){
                fichaMovil.setImagen(RUTA_FICHA_RED);
                Sound sound = ResourcesManager.obtenerSonido(FILE_CARTA_YELLOW);
                sound.play();
            }
            else if(rectMouse.overlaps(fichaAmarilla) && fichaMovil.getImagen().getRegionWidth() == 1) {
                fichaMovil.setImagen(RUTA_FICHA_YELLOW);
                Sound sound = ResourcesManager.obtenerSonido(FILE_CARTA_YELLOW);
                sound.play();
            }
            if(fichaMovil.getImagen().getRegionWidth() > 1){
                for(Ficha ficha: fichas){
                    if (ficha.rect.overlaps(rectMouse) && !ficha.cambiada){
                        ficha.setImagen(fichaMovil.getImagen());
                        tocando = false;
                        fichaMovil.setImagen("");
                        fichaMovil.posicion.x = 0;
                        fichaMovil.posicion.y = 0;
                        Sound sound = ResourcesManager.obtenerSonido(FILE_CARTA_OUT);
                        sound.play();
                    }
                }
            }
            if (tocando){
                fichaMovil.posicion.x = rectMouse.getX() - fichaMovil.getImagen().getRegionWidth() / 2;
                fichaMovil.posicion.y = rectMouse.getY() - fichaMovil.getImagen().getRegionWidth() / 2;
            }
        }
        else{
            tocando = false;
            fichaMovil.setImagen("");
            fichaMovil.posicion.x = 0;
            fichaMovil.posicion.y = 0;
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
