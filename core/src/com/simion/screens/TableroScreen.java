package com.simion.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisImage;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.toast.Toast;
import com.simion.Lanzador;
import com.simion.base.Caja;
import com.simion.resources.ResourcesManager;
import javafx.scene.control.TablePositionBuilder;

import static com.simion.util.Constantes.*;

/**
 * Created by Simion on 28/05/2017.
 */
public class TableroScreen implements Screen {
    private final Lanzador juego;
    private Batch batch;
    BitmapFont fuente;
    TiledMap mapa;
    OrthogonalTiledMapRenderer mapRenderer;
    Array<Caja> cajas;
    private OrthographicCamera camara;
    private Stage escena;
    private Rectangle rectMouse;
    private boolean permisoTocar;
    private VisTable tablaFin;
    private int numDescubiertos;

    public TableroScreen (Lanzador juego){
        this.juego = juego;
        batch = juego.batch;
    }

    @Override
    public void show() {
        camara = new OrthographicCamera();
        camara.setToOrtho(false, 1024, 768);
        camara.update();

        cajas = new Array<Caja>();

        mapa = new TmxMapLoader().load(RUTA_TABLERO);
        mapRenderer = new OrthogonalTiledMapRenderer(mapa);
        batch = mapRenderer.getBatch();

        mapRenderer.setView(camara);

        numDescubiertos = 0;

        MapLayer capaObjetos = mapa.getLayers().get("Cajas");
        MapObjects mapObjects = capaObjetos.getObjects();
        for (MapObject mapObject : mapObjects) {
            Rectangle rect = ((RectangleMapObject) mapObject).getRectangle();
            Caja caja = new Caja(rect.x, rect.y, RUTA_CAJA_NEGRA, (int) rect.width, (int) rect.height);
            cajas.add(caja);
        }

        escena = new Stage();
        VisTable tabla = new VisTable();
        tabla.setPosition(30, Gdx.graphics.getHeight() - 30);
        VisTable tabla2 = new VisTable();
        tabla2. setPosition(140, Gdx.graphics.getHeight() - 30);
        escena.addActor(tabla);
        escena.addActor(tabla2);
        rectMouse = new Rectangle(0,0,10,10);

        Drawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_ATRAS)));
        VisImageButton btAtras = new VisImageButton(drawable);
        btAtras.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new MenuScreen(juego));
            }
        });
        tabla.row();
        tabla.add(btAtras).center().pad(5).width(45).height(45);

        VisTextButton btReiniciar = new VisTextButton("Reiniciar");
        btReiniciar.setSize(60, 60);
        btReiniciar.setColor(com.badlogic.gdx.graphics.Color.WHITE);
        btReiniciar.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new TableroScreen(juego));
            }
        });
        tabla2.row();
        tabla2.add(btReiniciar).right().pad(5).width(150).height(45);

        tablaFin = new VisTable();
        tablaFin.setFillParent(true);
        // Añade la tabla a la escena
        escena.addActor(tablaFin);
        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_FIN_RETO)));
        VisImage vsFinImagen = new VisImage(drawable);
        VisTextButton btRestart = new VisTextButton("Reiniciar Partida");
        btRestart.setSize(200, 60);
        btRestart.setPosition(tabla.getWidth()/2 - btRestart.getWidth()/2, 40);
        btRestart.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new TableroScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });
        VisTextButton btMenuInicio = new VisTextButton("Volver al Menu Principal");
        btMenuInicio.setSize(200, 60);
        btMenuInicio.setPosition(tabla.getWidth()/2 - btMenuInicio.getWidth()/2, 40);
        btMenuInicio.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                juego.setScreen(new MenuScreen(juego));
                // Qué hacer al pulsar el botón
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });
        tablaFin.row();
        tablaFin.add(vsFinImagen).center().pad(5);
        tablaFin.row();
        tablaFin.add(btRestart).center().pad(5).width(300).height(80);;
        tablaFin.row();
        tablaFin.add(btMenuInicio).center().pad(5).width(300).height(80);;
        tablaFin.setVisible(false);

        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_AYUDA_TB)));
        final VisImageButton imagenAyuda = new VisImageButton(drawable);
        imagenAyuda.setSize(656, 520);
        imagenAyuda.setPosition(Gdx.graphics.getWidth()/2 - imagenAyuda.getWidth()/2, Gdx.graphics.getHeight()/2 - imagenAyuda.getHeight()/2);
        imagenAyuda.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                imagenAyuda.setVisible(false);
            }
        });
        imagenAyuda.setVisible(false);
        drawable = new TextureRegionDrawable(new TextureRegion(new Texture(RUTA_BT_AYUDA), 100, 100));
        VisImageButton btAyuda = new VisImageButton(drawable);
        btAyuda.setSize(45, 45);
        btAyuda.setPosition(Gdx.graphics.getWidth()/2 - btAyuda.getWidth()/2, Gdx.graphics.getHeight()- btAyuda.getHeight() - 10);
        btAyuda.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                imagenAyuda.setVisible(true);
            }
        });

        VisTable tablaAyuda = new VisTable();
        tablaAyuda.setPosition(Gdx.graphics.getWidth() - 30, Gdx.graphics.getHeight()-30);
        tablaAyuda.add(btAyuda).width(45).height(45);
        escena.addActor(tablaAyuda);
        escena.addActor(imagenAyuda);

        Gdx.input.setInputProcessor(escena);
    }

    @Override
    public void render(float delta) { Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mapRenderer.render();
        batch.begin();
        //batch.draw(foto, rectMouse.x, rectMouse.y);
        for(Caja caja: cajas){
            batch.draw(caja.getImagen(), caja.rect.x, caja.rect.y);
        }
        batch.end();
        escena.act(delta);
        escena.draw();

        if (numDescubiertos < 100){
            comprobarInput(delta);
            calcularTiempo();
        }
        else {
            tablaFin.setVisible(true);
        }
        if (!permisoTocar){
            permisoTocar = true;
        }
    }

    private void calcularTiempo() {
        for(Caja caja:cajas){
            if(caja.amarilla && System.currentTimeMillis() - caja.tiempo > 3000){
                caja.setImagen(new TextureRegion(new Texture(RUTA_CAJA_NEGRA), (int)caja.rect.getWidth(), (int)caja.rect.getHeight()));
                caja.amarilla = false;
            }
        }
    }

    private void comprobarInput(float delta) {
        if (Gdx.input.justTouched() && permisoTocar){
//            int touchX = Gdx.input.getX();
//            int touchY = Gdx.graphics.getHeight() - Gdx.input.getY();

            Vector3 posicion = new Vector3();
            posicion.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camara.unproject(posicion);
            rectMouse = new Rectangle(posicion.x - 5, posicion.y - 5, 10, 10);
            for (Caja caja: cajas){
                float tiempoPasado = System.currentTimeMillis() - caja.tiempo + delta;
                if (caja.rect.overlaps(rectMouse) && (tiempoPasado > 3000)){
                    caja.setImagen(new TextureRegion(new Texture(RUTA_CAJA_AMARILLA), (int)caja.rect.width, (int)caja.rect.height));
                    caja.tiempo = System.currentTimeMillis();
                    caja.amarilla = true;
                    Sound sound = ResourcesManager.obtenerSonido(FILE_CARTA_YELLOW);
                    sound.play();
                }
                else if(caja.rect.overlaps(rectMouse) && caja.amarilla && tiempoPasado <= 3000){
                    cajas.removeValue(caja, true);
                    numDescubiertos++;
                    Sound sound = ResourcesManager.obtenerSonido(FILE_CARTA_OUT);
                    sound.play();
                }
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
            dispose();
            juego.setScreen(new MenuScreen(juego));
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        VisUI.dispose();
    }
}

