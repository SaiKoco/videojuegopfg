package com.simion.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.utils.Array;

import javax.xml.soap.Text;

import static com.simion.util.Constantes.*;

/**
 * Created by Simion on 09/03/2017.
 */
public class ResourcesManager {

    private static AssetManager assetManager = new AssetManager();

    /**
     * Carga todos los assets del juego
     */
    public static void cargarRecursos(){

        assetManager.load(PK_ATLAS, TextureAtlas.class);
        assetManager.load(RUTA_CAJA_AMARILLA, Texture.class);
        assetManager.load(RUTA_CAJA_NEGRA, Texture.class);
        assetManager.load(RUTA_BOLA_BLANCA, Texture.class);
        assetManager.load(RUTA_BOLA_ROJA, Texture.class);
        assetManager.load(RUTA_BT_ADD, Texture.class);
        assetManager.load(RUTA_BT_ATRAS, Texture.class);
        assetManager.load(RUTA_BT_MINUS, Texture.class);
        assetManager.load(RUTA_FICHA_RED, Texture.class);
        assetManager.load(RUTA_FICHA_YELLOW, Texture.class);
        assetManager.load(FILE_CARTA_YELLOW, Sound.class);
        assetManager.load(RUTA_BT_AYUDA, Texture.class);
        assetManager.load(RUTA_BT_ADD, Texture.class);
        assetManager.load(RUTA_BT_MINUS, Texture.class);
        assetManager.load(RUTA_BT_ATRAS, Texture.class);
        assetManager.load(RUTA_AYUDA_AB, Texture.class);
        assetManager.load(RUTA_AYUDA_TB, Texture.class);
        assetManager.load(RUTA_AYUDA_FH, Texture.class);
        assetManager.load(RUTA_ERROR_RETO, Texture.class);
        assetManager.load(RUTA_FIN_RETO, Texture.class);
        assetManager.load(RUTA_TITULO, Texture.class);
        assetManager.load(RUTA_CREDITOS_TEXTO, Texture.class);
        assetManager.load(RUTA_JUEGO_ABACO, Texture.class);
        assetManager.load(RUTA_JUEGO_TABLERO, Texture.class);
        assetManager.load(RUTA_JUEGO_FICHAS, Texture.class);

        assetManager.load(FILE_CARTA_OUT, Sound.class);
        assetManager.load(FILE_MUSIC, Music.class);
        assetManager.finishLoading();
    }

    public static Music obtenerMusica(String nombreMusica){
        Music music = assetManager.get(nombreMusica,Music.class);

        return music;
    }

    public static Sound obtenerSonido(String nombreSonido){
        Sound sound = assetManager.get(nombreSonido, Sound.class);

        return sound;
    }
}
