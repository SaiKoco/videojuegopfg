package com.simion.util;

import org.omg.CORBA.PUBLIC_MEMBER;
import sun.awt.AWTAccessor;

/**
 * Created by Simion on 09/03/2017.
 */
public class Constantes {

    public static final String PK_ATLAS = "abaco/abaco.pack";
    public static final String FONDO_MAIN = "fondo.tmx";
    public static final String RUTA_CREDITOS = "creditos.tmx";
    public static final String RUTA_CREDITOS_TEXTO = "creditosTexto.png";

    public static final String RUTA_BOLA_ROJA = "abaco/bola_roja.png";
    public static final String RUTA_BOLA_BLANCA = "abaco/bola_blanca.png";
    public static final String RUTA_ABACO = "abaco/abaco.tmx";
    public static final String RUTA_AYUDA_AB = "abaco/ayuda.jpg";

    public static final String RUTA_CAJA_NEGRA = "tablero100/negro.png";
    public static final String RUTA_CAJA_AMARILLA = "tablero100/amarillo.png";
    public static final String RUTA_TABLERO = "tablero100/tablero.tmx";
    public static final String RUTA_AYUDA_TB = "tablero100/ayuda.jpg";

    public static final String RUTA_FICHA_RED = "fichasColores/ficha_roja.png";
    public static final String RUTA_FICHA_YELLOW = "fichasColores/ficha_amarilla.png";
    public static final String RUTA_TABFICHAS = "fichasColores/tabFichas.tmx";
    public static final String RUTA_AYUDA_FH = "fichasColores/ayuda.jpg";

    public static final String RUTA_BT_ATRAS = "atras.png";
    public static final String RUTA_FIN_RETO = "Felicidades.png";
    public static final String RUTA_ERROR_RETO = "HasFallado.png";
    public static final String RUTA_BT_ADD = "add.png";
    public static final String RUTA_BT_MINUS = "restar.png";
    public static final String RUTA_BT_AYUDA = "btAyuda.png";

    public static final String FICHA_ROJA = "FichaRoja";
    public static final String FICHA_AMARILLA = "FichaAmarilla";

    public static final String CP_COLISION = "Colision";

    public static final String RUTA_JUEGO_ABACO = "juegoAbaco.jpg";
    public static final String RUTA_JUEGO_TABLERO = "juegoTablero.jpg";
    public static final String RUTA_JUEGO_FICHAS = "juegoFichas.jpg";
    public static final String RUTA_TITULO = "VideoEduc.png";


    public static final String _1024x768 = "1024x768";
    public static final String _800x600 = "800x600";
    public static final String _640x480 = "640x480";

    public static int ANCHURA = 800;
    public static int ALTURA = 600;

    public static final int ANCHURA_CAMARA = 67;
    public static final int ALTURA_CAMARA = 50;
    public static final int SIZE_TILED = 20;

    public static final String FILE_MUSIC = "MenuPrincipal.mp3";
    public static final String FILE_CARTA_YELLOW = "carta_yellow.mp3";
    public static final String FILE_CARTA_OUT = "carta_out.mp3";
}
