package com.simion;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.simion.resources.ResourcesManager;
import com.simion.screens.MenuScreen;

import static com.simion.util.Constantes.*;

public class Lanzador extends Game {
	public SpriteBatch batch;

	@Override
	public void create () {
		ResourcesManager.cargarRecursos();

		batch = new SpriteBatch();

		setScreen(new MenuScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
