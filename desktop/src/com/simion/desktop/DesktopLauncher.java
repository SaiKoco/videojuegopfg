package com.simion.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.simion.Lanzador;
import com.simion.util.Constantes;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "VideoJuegos Educativos";
		config.height = Constantes.ALTURA;
		config.width = Constantes.ANCHURA;
		new LwjglApplication(new Lanzador(), config);
	}
}
